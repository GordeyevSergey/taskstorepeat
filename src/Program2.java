import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] massive = {27, 455, 17, 48, 32, 20};
        System.out.println("Введите No- элемента массива \n от 1 до " + massive.length);
        int number;
        do {
            number = in.nextInt();
        }while (number < 1 || number > massive.length);

        System.out.println(massive[number-1] + "  * 10% = " + String.format( "%.2f", massive[number-1] * 0.1));
    }
}
