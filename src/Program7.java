
import java.util.Scanner;
public class Program7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int target = (int) (Math.random() * 30);
        System.out.println(target);
        int number;
        System.out.println("Введите число");
        do {
            number = in.nextInt();
            if (number > target) {
                System.out.println("Загаданное число меньше");
            } else if (number < target) {
                System.out.println("Загаданное число больше");
            }
        } while (number != target);
        System.out.println("Правильно");
    }
}
