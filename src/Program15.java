/**
 * Класс, меняющий местами столбцы и строки в двумерном массиве
 */
public class Program15 {
    public static void main(String[] args){
        int line = 5;
        int[][] massive = new int[line][line];
        int[][] result = new int[line][line];
        inputMassive(massive);
        toPrint(massive);
        result = swap(massive);
        System.out.println();
        toPrint(result);
    }

    private static void inputMassive(int[][] massive) {
        for (int i = 0; massive.length > i; i++) {
            for (int j = 0; massive[i].length > j; j++) {
                massive[i][j] = (int) (Math.random() * 200);
            }
        }
    }

    public static int[][] swap(int[][] massive) {
        int[][] b = new int[massive.length][massive.length];
        for (int i = 0; i < massive.length; ++i)
            for (int j = 0; j < massive.length; ++j) {
                b[i][j] = massive[j][i];
            }
        return b;
    }

    private static void toPrint(int[][] massive) {
        for (int i = 0; i < massive.length; i++) {
            for (int j = 0; j < massive[i].length; j++) {
                System.out.print(massive[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
