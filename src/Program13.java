/**
 * Класс переносит построчно элементы двумерного массива в одномерный
 */
public class Program13 {
    public static void main(String[] args){
        int line = 4;
        int columns = 6;
        int[][] massive = new int[line][columns];
        int[] newMassive = new int[line*columns];
        inputMassive(massive);
        toPrint(massive);
        transfer(massive,newMassive);
        toPrint(newMassive);

    }

    private static void inputMassive(int[][] massive) {
        for (int i = 0; massive.length > i; i++) {
            for (int j = 0; massive[i].length > j; j++) {
                massive[i][j] = (int) (Math.random() * 200);
            }
        }
    }

    private static void toPrint(int[][] massive) {
        for (int i = 0; i < massive.length; i++) {
            for (int j = 0; j < massive[i].length; j++) {
                System.out.print(massive[i][j] + "  ");
            }
            System.out.println();
        }
    }

    private static void toPrint(int[] massive) {
        for (int i = 0; i < massive.length; i++) {
                System.out.print(massive[i] + "  ");
            }
        }


    private static void transfer(int[][] massive, int[] newMassive){
        int counter = 0;
        for (int i = 0; i < massive.length; i++) {
            for (int j = 0; j < massive[i].length; j++) {
                newMassive[counter] = massive[i][j];
                counter = counter+1;
            }
        }
    }
}
