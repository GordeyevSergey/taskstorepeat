import java.util.Scanner;

public class Program12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введиите кол-во суток");
        int days = in.nextInt();
        System.out.println("Кол-во часов = " + days * 24 + " ч." +
                "\n" + "Кол-во минут = " + days * 1440 + " мин." +
                "\n" + "Кол-во секунд = " + days * 86400 + " сек.");
    }
}
