import java.util.Scanner;

public class Program4 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Введите интервал между вспышкой молнии и звуком (В секундах)");
        double interval = in.nextInt();
        System.out.println("Расстояние до удара молнии = " + String.format( "%.2f",interval/3600 * 1234.8) + " км.");
    }
}
