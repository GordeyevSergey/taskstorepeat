import java.util.Scanner;

/**
 * Класс, выводящий на экран таблицу умножения вводимого числа
 */
public class Program6 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число");
        int number = in.nextInt();
        for (int i=0; i<11; i++){
            System.out.println(number + " * " + i + " = " + number*i);
        }

    }
}
