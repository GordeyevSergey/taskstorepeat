import java.util.Scanner;

/**
 * Класс, для проверки заданного числа
 * Результатом является вывод сообщения на экран Целое \ Дробное
 */
public class Program8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число для проверки");
        double number = in.nextDouble();
        if (number % 1 == 0) {
            System.out.println("Число целое");
        } else {
            System.out.println("Число дробное");
        }
    }
}
