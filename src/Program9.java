import java.util.Scanner;

/**
 * Класс, обнуляющий заданный столбец прямоугольной матрицы
 */
public class Program9 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number;
        int line = 3;
        int columns = 8;
        int[][] massive = new int[line][columns];
        inputMassive(massive);
        toPrint(massive);
        do {
            System.out.println("Введите номер обнуляемого столбца");
            number = in.nextInt();
        } while (number < 0 || number > columns);
        nullMassive(massive, number);
        toPrint(massive);
    }

    /**
     * Метод заполнения двумерного массива
     * @param massive двумерный массив
     */
    private static void inputMassive(int[][] massive) {
        for (int i = 0; massive.length > i; i++) {
            for (int j = 0; massive[i].length > j; j++) {
                massive[i][j] = (int) (Math.random() * 200);
            }
        }
    }

    /**
     * Метод вывода двумерного массива на экран
     * @param massive двумерный массив
     */
    private static void toPrint(int[][] massive) {
        for (int i = 0; i < massive.length; i++) {
            for (int j = 0; j < massive[i].length; j++) {
                System.out.print(massive[i][j] + "  ");
            }
            System.out.println();
        }
    }

    /**
     * Метод, обнуляющий заданный столбец в двумерном массиве
     * @param massive двумерный массив
     * @param number номер обнуляемоого столбца
     */
    private static void nullMassive(int[][] massive, int number) {
        for (int i = 0; i < massive.length; i++) {
            massive[i][number - 1] = 0;
        }
    }
}